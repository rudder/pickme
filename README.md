# Ethereum Swarm
### How to use pickme swarm
#### Install dependencies package
```
sudo apt-get install npm
sudo npm install -g yarn
yarn add web3 web3-bzz
```

#### How to use upload/download of swarm
```
node upload.js $PWD/upload-test
node download.js [Hash address from the above upload command]
```

# History
### Set Env
#### Ubuntu Prerequisites
```
sudo apt install git curl
curl -O https://storage.googleapis.com/golang/go1.9.2.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf go1.9.2.linux-amd64.tar.gz
mkdir -p ~/go; echo "export GOPATH=$HOME/go" >> ~/.bashrc
echo "export PATH=$PATH:$HOME/go/bin:/usr/local/go/bin" >> ~/.bashrc
source ~/.bashrc
```


#### Installing from source
```
mkdir -p $GOPATH/src/github.com/ethereum
cd $GOPATH/src/github.com/ethereum
git clone https://github.com/ethereum/go-ethereum
cd go-ethereum
git checkout master
go get github.com/ethereum/go-ethereum

go install -v ./cmd/geth
go install -v ./cmd/swarm

$GOPATH/bin/swarm version
```


#### Updating your client
```
cd $GOPATH/src/github.com/ethereum/go-ethereum
git checkout master
git pull
go install -v ./cmd/geth
go install -v ./cmd/swarm
```


### Run Swarm
#### Create Account
geth --testnet --datadir /home/ubuntu/pickme/swarm/datadir account new

#### Run Geth
geth --testnet --fast --datadir /home/ubuntu/pickme/swarm/datadir

#### Run Swarm
export BZZKEY='d84fe6e3e1b5e26dde68a9cb01829553d1d54947'
swarm --bzzaccount $BZZKEY --datadir /home/ubuntu/pickme/swarm/datadir


### Set Reverse Proxy
#### Install App
sudo apt install -y nginx
sudo vi /etc/nginx/sites-available/default
```
server {
        listen 80 default_server;
        listen [::]:80 default_server;

        root /var/www/html;
        index index.html index.htm index.nginx-debian.html;
        server_name _;

        location / {
                proxy_pass http://localhost:8500;
        }
}
```
sudo /etc/init.d/nginx reload

### Reference Site
#### ethereum swarm
- http://swarm-guide.readthedocs.io/en/latest/introduction.html
- https://web3js.readthedocs.io/en/1.0/web3-bzz.html#
- https://medium.com/@codeAMT/how-to-launch-swarm-for-dapp-testing-8003e55380e2
- http://samse.tistory.com/entry/dapp%ED%85%8C%EC%8A%A4%ED%8A%B8%EB%A5%BC-%EC%9C%84%ED%95%9C-Swarm-%EC%8B%A4%ED%96%89%ED%95%98%EA%B8%B0
- https://github.com/ethersphere/swarm-guide/blob/master/contents/usage.rst

#### proxy
- https://ethereum.stackexchange.com/questions/8187/how-to-run-a-swarm-gateway/10955
- http://interconnection.tistory.com/27
- https://www.joinc.co.kr/w/man/12/proxy
- https://www.joinc.co.kr/w/Site/System_management/Proxy

#### 
