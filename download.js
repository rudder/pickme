const SWARM_SERVER = "http://18.220.252.229/";

const path = require("path");
const Bzz = require('web3-bzz');
var bzz = new Bzz(SWARM_SERVER);

if(process.argv.length != 3){
    console.log("node", process.argv[1], "[Bzz Hash Address to download]");
    return;
}

// download to disk in node.js
var data_hash = process.argv[2];
var download_path = path.join(__dirname, "/download");

bzz.download(data_hash, download_path)
.then(path => console.log("SUCCESS!!", `Downloaded directory to "${path}"`))
.catch(err => console.log("ERROR!!", err));
