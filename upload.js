const SWARM_SERVER = "http://18.220.252.229/";

var Bzz = require('web3-bzz');
var bzz = new Bzz(SWARM_SERVER);

if(process.argv.length != 3){
    console.log("node", process.argv[1], "[Full-path to upload]");
    return;
}

// upload from disk in node.js
var upload_path = process.argv[2];
bzz.upload({
    path: upload_path,                           // path to data / file / directory
    kind: "directory",                           // could also be "file" or "data"
    defaultFile: "/just-do-it-wallpaper.jpg"     // optional, and only for kind === "directory"
})
.then(hash => console.log("SUCCESS!!", hash))
.catch(err => console.log("ERROR!!", err));
